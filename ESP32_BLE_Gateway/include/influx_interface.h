#include <InfluxDbClient.h>
#include <ArduinoJson.h>
#include "debug_interface.h"
#include "gateway_config.h"

void writePointsToInflux(DynamicJsonDocument &json);