#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <ArduinoJson.h>
#include "debug_interface.h"

bool setupSdCard();
void printSdInfo(uint8_t cardType);
void writeHeader(const char *path, const char *headerContent);
void writeCsvHeaderFromJsonToSd(const char *path, DynamicJsonDocument &json);
void writeCsvLineFromJsonToSd(const char *path, uint32_t now_ts, DynamicJsonDocument &json);