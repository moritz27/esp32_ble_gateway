#include <Arduino.h>
#include <ArduinoJson.h>
#include "gateway_config.h"
#include "debug_interface.h"

#if defined(BLE_DEVICE_B35T)
#define DEVICENAME "BDM"                                            // OWON device name
#define serviceUUID "0000fff0-0000-1000-8000-00805f9b34fb"          // OWON service UUID
#define charnotificationUUID "0000fff4-0000-1000-8000-00805f9b34fb" // OWON notification characteristic UUID
#define charwriteUUID "0000fff3-0000-1000-8000-00805f9b34fb"        // OWON write characteristic UUID
#define replySize 14                                                // number of bytes in meter reply

#elif defined(BLE_DEVICE_ALLPOWERS)
#define DEVICENAME "AP S300 V2.0"                                  // device name
#define serviceUUID "0000fff0-0000-1000-8000-00805f9b34fb"          // service UUID
#define charnotificationUUID "0000fff1-0000-1000-8000-00805f9b34fb" // notification characteristic UUID
#define charwriteUUID "0000fff2-0000-1000-8000-00805f9b34fb"        // write characteristic UUID
#define replySize 16                                                // number of bytes in meter reply
#endif

void initValues(DynamicJsonDocument &json);
bool parseValues(uint8_t replyBuffer[], DynamicJsonDocument &json);