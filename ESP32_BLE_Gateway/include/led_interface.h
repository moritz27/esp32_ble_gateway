#include "Arduino.h"
#include "debug_interface.h"

#if defined(LED_DATA_PIN)
#include <FastLED.h>
void setLedColor(CRGB color);
#endif

void setupLed();
void startLedBlinkTimer(uint16_t intervalMS);
void checkLedstate();
void breatheLed();