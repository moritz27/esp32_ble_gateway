#include <PubSubClient.h>
#include <ArduinoJson.h>
#include "debug_interface.h"

void setup_mqtt(PubSubClient &client, const char *hostname, const uint8_t retries = 5);
uint32_t send_mqtt_string(PubSubClient &client, const char *hostname, const char *content, const char *topic, const uint8_t retries = 5);
uint32_t send_mqtt_json(PubSubClient &client, const char *hostname, DynamicJsonDocument &json, const char *topic, const uint8_t retries = 5);
