#include <Adafruit_GFX.h>
#include <Adafruit_ST7789.h>
#include <SPI.h>

void setupDisplay(const char *header);
void clearText();
void reprintText(const char *newText);