#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "gateway_config.h"
#include "debug_interface.h"

void setup_OTA(const char* hostname);