#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEAdvertisedDevice.h>
#include <BLEClient.h>
#include <BLEScan.h>
#include <BLEUtils.h>
#include "gateway_config.h"
#include "debug_interface.h"
#include "ble_devices.h"

#define maxWaitForNotify 10000 // max wait time for next notify in ms
#define scanTime 10            // BLE scan time in s

bool connectToServer();
void doScan();
bool getConnectionStatus();
bool newBleDataAvailable();
void resetBleDataAvailable();
unsigned long getLastBleNotifyTime();
void setLastBleNotifyTime(unsigned long value);
unsigned long getScanStartTime();
void setScan(bool status);
uint8_t* getReplyBuffer();
