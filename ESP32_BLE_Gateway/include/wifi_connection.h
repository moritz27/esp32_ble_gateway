#include "gateway_config.h"
#include "helper.h"

#if defined(ESP8266)
#include <ESP8266WiFi.h>
#elif defined(ESP32)
#include <WiFi.h>
#if defined(USE_WIFI_MULTI)
#include <WiFiMulti.h>
void setup_wifi_multi(WiFiMulti wifiMulti, const uint8_t retries = 10);
#else
void setup_wifi(WiFiClient &wifi_client, const char *ssid, const char *wifi_password, const uint8_t retries = 10);
#endif
#else
#error "This ain't a ESP8266 or ESP32, dumbo!"
#endif

#include "debug_interface.h"
