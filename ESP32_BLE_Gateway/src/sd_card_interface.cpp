#include "sd_card_interface.h"

SPIClass spi = SPIClass();

bool setupSdCard()
{
    DEBUG_MSG_SDCARD("Init SD Card\n");
    spi.begin(SD_SCK, SD_MISO, SD_MOSI, SD_CS);
    if (!SD.begin(SD_CS, spi, 80000000))
    {
        DEBUG_MSG_SDCARD("Card Mount Failed\n");
        return false;
    }
    uint8_t cardType = SD.cardType();
    if (cardType == CARD_NONE)
    {
        DEBUG_MSG_SDCARD("No SD card attached\n");
        return false;
    }
    printSdInfo(cardType);
    return true;
}

void printSdInfo(uint8_t cardType)
{
    DEBUG_MSG_SDCARD("SD Card Type: ");
    if (cardType == CARD_MMC)
    {
        DEBUG_MSG_SDCARD("MMC\n");
    }
    else if (cardType == CARD_SD)
    {
        DEBUG_MSG_SDCARD("SDSC\n");
    }
    else if (cardType == CARD_SDHC)
    {
        DEBUG_MSG_SDCARD("SDHC\n");
    }
    else
    {
        DEBUG_MSG_SDCARD("UNKNOWN\n");
    }
    DEBUG_MSG_SDCARD("Used Space: %lluMB of %lluMB\n", SD.usedBytes() / (1024 * 1024), SD.totalBytes() / (1024 * 1024));
}

void writeHeader(const char *path, const char *headerContent)
{
    File file = SD.open(path);
    if (!file)
    {
        DEBUG_MSG_SDCARD("File doesn't exist. Creating file and Header...\n");
        file = SD.open(path, FILE_WRITE);
        file.print(headerContent);
        file.print("\n");
    }
    else
    {
        DEBUG_MSG_SDCARD("File already exists\n");
    }
    file.close();
}

void writeCsvHeaderFromJsonToSd(const char *path, DynamicJsonDocument &json)
{
    String headerContent = "Timestamp";
    JsonObject attributes = json["Attributes"].as<JsonObject>();
    for (JsonPair keyValue : attributes)
    {
        headerContent += ";";
        headerContent += keyValue.key().c_str();
    }

    JsonObject values = json["Values"].as<JsonObject>();
    for (JsonPair keyValue : values)
    {
        headerContent += ";";
        headerContent += keyValue.key().c_str();
    }
    DEBUG_MSG_SDCARD("%s \n", headerContent);
    writeHeader(path, headerContent.c_str());
}

void writeLineToSd(const char *path, const char *content)
{
    File file = SD.open(path, FILE_APPEND);
    file.println(content);
    file.close();
}
void writeCsvLineFromJsonToSd(const char *path, uint32_t now_ts, DynamicJsonDocument &json)
{
    String lineContent = String(now_ts);

    JsonObject attributes = json["Attributes"].as<JsonObject>();
    for (JsonPair keyValue : attributes)
    {
        // DEBUG_MSG_SDCARD("%s", keyValue.key().c_str());
        lineContent += ";";
        lineContent += (const char *)keyValue.value();
    }

    JsonObject values = json["Values"].as<JsonObject>();
    for (JsonPair keyValue : values)
    {
        // DEBUG_MSG_SDCARD("%s", keyValue.key().c_str());
        lineContent += ";";
        lineContent += String(keyValue.value().as<float>(), 6);
    }
    DEBUG_MSG_SDCARD("%s \n", lineContent.c_str());
    writeLineToSd(path, lineContent.c_str());
}