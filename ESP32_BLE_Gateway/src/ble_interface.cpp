#include "ble_interface.h"

static BLEAddress *pServerAddress;
static BLERemoteCharacteristic *pRemoteCharacteristicNotify;
static BLERemoteCharacteristic *pRemoteCharacteristicWrite;

volatile boolean deviceBleConnected = false; // flag BLE connected
volatile boolean newBleData = false;         // flag "we get new datar"
static unsigned long lastBleNotify = 0;      // timestamp "last received data" in ms
static unsigned long startBleScanning = 0;   // timestamp when ble scan is beginning in ms

uint8_t replyBuffer[replySize]; // meter reply buffer

static void notifyCallback(BLERemoteCharacteristic *pBLERemoteCharacteristic, uint8_t *pData, size_t length, bool isNotify)
{

  if (isNotify == true && length == replySize && pBLERemoteCharacteristic->getUUID().equals(BLEUUID(charnotificationUUID)))
  {

    //    DEBUG_MSG_BLE_INTERFACE("Notify callback len=%d (UUID: %s)\n", length, pBLERemoteCharacteristic->getUUID().toString().c_str());

    if (memcmp(replyBuffer, pData, replySize) != 0)
    { // if new data <> old data
      if (newBleData == false)
      { // and if old data are displayed then copy new data
        memcpy(replyBuffer, pData, replySize);
        /*
                for (uint8_t i = 0; i < replySize; i++) {
                  DEBUG_MSG_BLE_INTERFACE("%02X ", replyBuffer[i]);
                }
                DEBUG_MSG_BLE_INTERFACE("\n");
        */
        newBleData = true;
      }
    }
    lastBleNotify = millis();
  }
}

class MyClientCallbacks : public BLEClientCallbacks
{
  void onConnect(BLEClient *pClient)
  {
    deviceBleConnected = true; // set ble connected flag
    DEBUG_MSG_BLE_INTERFACE("%S connected \n", DEVICENAME);
  };

  void onDisconnect(BLEClient *pClient)
  {
    pClient->disconnect();
    deviceBleConnected = false; // clear ble connected flag
    DEBUG_MSG_BLE_INTERFACE("%S disconnected \n", DEVICENAME);
  }
};

bool connectToServer()
{

  DEBUG_MSG_BLE_INTERFACE("Create a connection to addr: %s\n", pServerAddress->toString().c_str());

  BLEClient *pClient = BLEDevice::createClient();

  DEBUG_MSG_BLE_INTERFACE(" - Client created\n");

  pClient->setClientCallbacks(new MyClientCallbacks());

  DEBUG_MSG_BLE_INTERFACE(" - Connecting to server...\n");

  pClient->connect(*pServerAddress); // connect to the remove BLE Server.

  BLERemoteService *pRemoteService = pClient->getService(serviceUUID); // check if remote BLE service exists
  if (pRemoteService == nullptr)
  {
    DEBUG_MSG_BLE_INTERFACE(" - Service not found (UUID: %s)\n", serviceUUID);
    return false;
  }
  else
  {
    DEBUG_MSG_BLE_INTERFACE(" - Service found (UUID: %s)\n", serviceUUID);
  }

  // notify characteristic
  pRemoteCharacteristicNotify = pRemoteService->getCharacteristic(charnotificationUUID);
  if (pRemoteCharacteristicNotify == nullptr)
  {
    DEBUG_MSG_BLE_INTERFACE(" - Notify characteristic not found (UUID: %s)\n", charnotificationUUID);
    return false;
  }
  else
  {
    DEBUG_MSG_BLE_INTERFACE(" - Notify characteristic found (UUID: %s)\n", charnotificationUUID);
  }
  pRemoteCharacteristicNotify->registerForNotify(notifyCallback); // register notify callback
  return true;
}

class MyAdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks
{

  void onResult(BLEAdvertisedDevice advertisedDevice)
  {
    DEBUG_MSG_BLE_INTERFACE("BLE scan stop\n");
    DEBUG_MSG_BLE_INTERFACE("%s \n", advertisedDevice.getName().c_str());
    if (advertisedDevice.haveName() && strcmp(advertisedDevice.getName().c_str(), DEVICENAME) == 0)
    {
      advertisedDevice.getScan()->stop();
      pServerAddress = new BLEAddress(advertisedDevice.getAddress());
      DEBUG_MSG_BLE_INTERFACE("BLE device found (%s at addr: %s)\n", DEVICENAME, pServerAddress->toString().c_str());
    }
    else
    {
      DEBUG_MSG_BLE_INTERFACE("BLE device not found\n");
    }
  }
};

void doScan()
{
  DEBUG_MSG_BLE_INTERFACE("BLE scan start\n");
  startBleScanning = millis();
  BLEScan *pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true);
  pBLEScan->start(scanTime);
}

bool getConnectionStatus(){
  return deviceBleConnected;
}

bool newBleDataAvailable(){
  return newBleData;
}

void resetBleDataAvailable(){
  newBleData = 0;
}

unsigned long getLastBleNotifyTime(){
  return lastBleNotify;
}

void setLastBleNotifyTime(unsigned long value){
  lastBleNotify = value;
}

unsigned long getScanStartTime(){
  return startBleScanning;
}

void setScan(bool status){
  startBleScanning = status;
}

uint8_t* getReplyBuffer(){
  return replyBuffer;
}