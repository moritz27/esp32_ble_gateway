#include "influx_interface.h"

#if defined(INFLUXDB)

InfluxDBClient influxClient(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);

void writePointsToInflux(DynamicJsonDocument &json)
{
    Point pointDevice(MEASUREMENT_NAME);
    pointDevice.clearFields();

    DEBUG_MSG_INFLUXDB("Tags: \n");
    JsonObject attributes = json["Attributes"].as<JsonObject>();
    for (JsonPair keyValue : attributes)
    {
        DEBUG_MSG_INFLUXDB("    %s: %s \n", keyValue.key().c_str(), (const char *)keyValue.value());
        pointDevice.addTag(keyValue.key().c_str(), (const char *)keyValue.value());
    }

    // pointDevice.addField("WIFI RSSI", WiFi.RSSI());

    DEBUG_MSG_INFLUXDB("Fields: \n");
    JsonObject values = json["Values"].as<JsonObject>();
    for (JsonPair keyValue : values)
    {
        const char* key = keyValue.key().c_str();
        auto value = json["Values"][key].as<float>();
        DEBUG_MSG_INFLUXDB("    %s: %4.4f \n", key, value);
        pointDevice.addField(key, value, 6);
    }
    
     DEBUG_MSG_INFLUXDB("Writing InfluxDB Point: %s \n", pointDevice.toLineProtocol().c_str());

    if (!influxClient.writePoint(pointDevice))
    {
        DEBUG_MSG_INFLUXDB("InfluxDB write failed: %s \n", influxClient.getLastErrorMessage().c_str());
    }
}

#endif