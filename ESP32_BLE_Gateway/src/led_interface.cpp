#include "led_interface.h"

#if defined(LED_DATA_PIN)
CRGB leds[1];
#endif
#if defined(LED_PIN) || defined(LED_DATA_PIN)
#if !defined(FADE_DELAY_INTERVALL)
#define FADE_DELAY_INTERVALL 10
#endif
#define FADE_AMOUNT 5

hw_timer_t *ledTimer = NULL;
uint8_t lastFadeCounter = 0;
volatile uint8_t fadeCounter = 0;
volatile bool fadeUp = 1;

volatile bool ledState = 0;
bool lastLedState = 0;

void ledSetOutput(uint8_t brightness = 150)
{
    if (ledState == 0)
    {
#if defined(LED_DATA_PIN)
        FastLED.setBrightness(0);
#else
        digitalWrite(LED_PIN, LOW);
#endif
    }
    else
    {
#if defined(LED_DATA_PIN)
        FastLED.setBrightness(brightness);
#else
        analogWrite(LED_PIN, brightness);
#endif
    }
#if defined(LED_DATA_PIN)
    FastLED.show();
#endif
}

void ledFadeRamp()
{
    if (fadeCounter >= 255)
    {
        fadeUp = 0;
    }
    else if (fadeCounter <= 0)
    {
        fadeUp = 1;
    }

    if (fadeUp == true)
    {
        fadeCounter += FADE_AMOUNT;
    }
    else
    {
        fadeCounter -= FADE_AMOUNT;
    }
}

void IRAM_ATTR onLedTimer()
{
    ledState = !ledState;
    ledFadeRamp();
}

void startLedTimer(uint16_t intervalMS)
{
    timerAlarmWrite(ledTimer, intervalMS * 1000, true);
    timerAlarmEnable(ledTimer);
}

void stopLed()
{
    timerAlarmDisable(ledTimer);
}

#if defined(LED_DATA_PIN)
void setLedColor(CRGB newColor)
{
    leds[0] = newColor;
    FastLED.show();
}
#endif

void setupLed()
{
#if LED_PIN
    pinMode(LED_PIN, OUTPUT);
#elif esp32_c3_pico
    FastLED.addLeds<WS2812B, LED_DATA_PIN, RGB>(leds, 1); // C3 Pico Board
#elif esp32_c3_oled
    FastLED.addLeds<NEOPIXEL, LED_DATA_PIN>(leds, 1); // C3 OLED Board
#endif
    ledTimer = timerBegin(0, 80, true);
    timerAttachInterrupt(ledTimer, &onLedTimer, true);
    startLedTimer(10);
}

void checkLedstate()
{
    if (ledState != lastLedState)
    {
        lastLedState = ledState;
        DEBUG_MSG_LED("ledState: %d lastLedState: %d\n", ledState, lastLedState);
        ledSetOutput();
    }
}

void breatheLed()
{
    if (lastFadeCounter != fadeCounter)
    {
        lastFadeCounter = fadeCounter;
#if LED_PIN
        analogWrite(LED_PIN, fadeCounter);
#elif LED_DATA_PIN
        FastLED.setBrightness(fadeCounter);
        FastLED.show();
#endif
    }
}

#else
void setupLed(){
    DEBUG_MSG_LED("No LED Pin defined.. exit");
}
#endif
