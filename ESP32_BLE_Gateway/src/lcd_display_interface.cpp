#include "lcd_display_interface.h"

Adafruit_ST7789 tft = Adafruit_ST7789(TFT_CS, TFT_DC, TFT_RST);
char *displayBuffer;

void setupDisplay(const char *header)
{
    tft.init(240, 320);
    delay(1);
    tft.invertDisplay(false);
    tft.fillScreen(ST77XX_BLACK);
    tft.setRotation(3);
    tft.setTextSize(3);
    tft.setCursor(0, 0);
    tft.setTextColor(ST77XX_WHITE);
    tft.print(header);
    tft.setTextSize(2);
}

void clearText()
{
    tft.setCursor(0, 30);
    tft.setTextWrap(true);
    tft.setTextColor(ST77XX_BLACK);
    tft.print(displayBuffer);
}

void reprintText(const char *newText)
{
    clearText();
    tft.setCursor(0, 30);
    tft.setTextColor(ST77XX_WHITE);
    displayBuffer = (char *)newText;
    tft.print(newText);
}