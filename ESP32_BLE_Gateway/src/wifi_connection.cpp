#include "wifi_connection.h"

#if defined(USE_WIFI) || defined(USE_WIFI_MULTI)
uint8_t doWifiScan()
{
  DEBUG_MSG_WIFI("Scanning WIFI.. ");
  int16_t n = WiFi.scanNetworks();
  DEBUG_MSG_WIFI(" done! \n");
  if (n == 0)
  {
    DEBUG_MSG_WIFI("No Networks found!");
  }
  else
  {
    DEBUG_MSG_WIFI("%d Networks found \n", n);
    for (int i = 0; i < n; ++i)
    {
      DEBUG_MSG_WIFI("  %d: %s (%d)\n", i + 1, WiFi.SSID(i).c_str(), WiFi.RSSI(i));
      delay(10);
    }
  }
  return n;
}
#endif

#if defined(USE_WIFI_MULTI)
void setup_wifi_multi(WiFiMulti wifiMulti, const uint8_t retries)
{
  WiFi.mode(WIFI_STA);

  doWifiScan();

  DEBUG_MSG_WIFI("Trying to connect to WIFI..\n");

#if defined(WIFI_SSID_DEFAULT) && defined(WIFI_PASSWORD_DEFAULT)
  wifiMulti.addAP(WIFI_SSID_DEFAULT, WIFI_PASSWORD_DEFAULT);
#endif

#if defined(WIFI_SSID_1) && defined(WIFI_PASSWORD_1)
  wifiMulti.addAP(WIFI_SSID_1, WIFI_PASSWORD_1);
#endif

#if defined(WIFI_SSID_2) && defined(WIFI_PASSWORD_2)
  wifiMulti.addAP(WIFI_SSID_2, WIFI_PASSWORD_2);
#endif

#if defined(WIFI_SSID_3) && defined(WIFI_PASSWORD_3)
  wifiMulti.addAP(WIFI_SSID_3, WIFI_PASSWORD_3);
#endif

#if defined(WIFI_SSID_4) && defined(WIFI_PASSWORD_4)
  wifiMulti.addAP(WIFI_SSID_4, WIFI_PASSWORD_4);
#endif

  if (wifiMulti.run(10000) == WL_CONNECTED)
  {
    IPAddress ip = WiFi.localIP();
    DEBUG_MSG_WIFI("WiFi connected to %s (%d), IP address: %d.%d.%d.%d \n", WiFi.SSID().c_str(), WiFi.RSSI(), ip[0], ip[1], ip[2], ip[3]);
  }
  else
  {
    DEBUG_MSG_WIFI("WIFI could not be connected");

#if defined(RESTART_IF_NO_WIFI)
    DEBUG_MSG_WIFI(" restarting ESP now..\n");
    delay(100);
    ESP.restart();
    delay(100);
#endif
    DEBUG_MSG_WIFI("\n");
  }
}
#elif defined(USE_WIFI)
void setup_wifi(WiFiClient &wifi_client, const char *ssid, const char *wifi_password, const uint8_t retries)
{
  WiFi.mode(WIFI_STA);
  // WiFi.setSleepMode(WIFI_NONE_SLEEP);
  // WiFi.begin(ssid, wifi_password);
  int success = false;
  DEBUG_MSG_WIFI("Setup WIFI \n");
  WiFi.begin(WIFI_SSID_DEFAULT, WIFI_PASSWORD_DEFAULT);
  for (int i = 0; i <= retries; i++)
  {
    if (WiFi.status() == WL_CONNECTED)
    {
      success = true;
      break;
    }
    else
    {
      delay(1000);
    }
  }
  if (success == true)
  {
    // Debugging - Output the IP Address of the ESP
    IPAddress ip = WiFi.localIP();
    DEBUG_MSG_WIFI("WiFi connected, IP address: %d.%d.%d.%d \n", ip[0], ip[1], ip[2], ip[3]);
  }
  else
  {
    DEBUG_MSG_WIFI("WIFI could not be connected, restarting now \n");
    delay(100);
    ESP.restart();
    delay(100);
  }
  return;
}
#endif