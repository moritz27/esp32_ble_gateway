#include "mqtt_interface.h"

void setup_mqtt(PubSubClient& mqttClient, const char* hostname, const uint8_t retries) {
  DEBUG_MSG_MQTT("Setup MQTT \n");
  int success = false;
  for (int i = 0; i <= retries; i++) {
    mqttClient.connect(hostname);
    if (mqttClient.connected()) {
      success = true;
      break;
    }
    if (success == true) {
      DEBUG_MSG_MQTT("MQTT connected \n");
    }
    else {
      DEBUG_MSG_MQTT("MQTT could not be connected, restarting now \n");
      delay(100);
      ESP.restart();
      delay(100);
    }
  }
  return;
}

uint32_t send_mqtt_string(PubSubClient& mqttClient, const char* hostname, const char* content, const char* topic, const uint8_t retries) {
  if (!mqttClient.connected()) {
    // DEBUG_MSG_MQTT("MQTT disconnected, reconnect now \n");
    mqttClient.connect(hostname);
    delay(100);
  }
  int success = false;
  for (int i = 0; i <= retries; i++) {
    DEBUG_MSG_MQTT("trying to send topic: %S \n", topic);
    if (mqttClient.publish(topic, content)) {
      success = true;
      break;
    }
    else {
      mqttClient.connect(hostname);
      delay(100);
    }
    delay(100);
  }
  if (success == true) {
    DEBUG_MSG_MQTT("%S message sent \n", topic);
    return micros();
  }
  else {
    // DEBUG_MSG_MQTT("%S message could not be sent, restarting now \n" , topic);
    delay(100);
    ESP.restart();
    delay(100);
    return 0;
  }
}

uint32_t send_mqtt_json(PubSubClient& mqttClient, const char* hostname, DynamicJsonDocument& json, const char* topic, const uint8_t retries) {
    String json_string;
    serializeJson(json, json_string);
    return send_mqtt_string(mqttClient, hostname, json_string.c_str(), topic);
}

