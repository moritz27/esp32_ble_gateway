
#include "ble_devices.h"

void printBuffer(uint8_t replyBuffer[])
{
  for (int8_t i = 0; i <= replySize; i++)
  {
    DEBUG_MSG_BLE_DEVICES("Byte %d: " BYTE_TO_BINARY_PATTERN "\n", i, BYTE_TO_BINARY(replyBuffer[i]));
  }
}

#if defined(BLE_DEVICE_B35T)
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*
  14 bytes reply
  -----------------------------------------------------------------------
  |  1 |         2         |  3 |  4 |  5 |  6 |    7    |  8 |    9    |
  -----------------------------------------------------------------------
  |0x2b|0x33 0x36 0x32 0x33|0x20|0x34|0x31|0x00|0x40 0x80|0x24|0x0d 0x0a|
  -----------------------------------------------------------------------
  |  0 |  1    2    3    4 |  5 |  6 |  7 |  8 |  9   10 | 11 | 12   13 |
  -----------------------------------------------------------------------
*/

/*
  1:  + or - (dec 43 or 45)
    BYTE 0
*/
#define REGPLUSMINUS 0x00
#define FLAGPLUS B00101011
#define FLAGMINUS B00101101

/*
  2:  Value 0000-9999 in dec
    BYTE 1-4
*/
#define REGDIG1 0x01
#define REGDIG2 0x02
#define REGDIG3 0x03
#define REGDIG4 0x04

/*
  3:  Just space (dec 32)
    BYTE 5
*/

/*
  4:  Decimal point position
    - dec 48 no point
    - dec 49 after the first number
    - dec 50 after the second number
    - dec 52 after the third number
    BYTE 6
*/
#define REGPOINT 0x06
#define FLAGPOINT0 B00110000
#define FLAGPOINT1 B00110001
#define FLAGPOINT2 B00110010
#define FLAGPOINT3 B00110100

/*
  5:  AC or DC and Auto mode
    - dec 49 DC Auto mode / 51 Hold
    - dec 41 AC Auto mode / 43 Hold
    - dec 17 DC Manual mode / 19 Hold
    - dec 09 AC Manual mode / 11 Hold
    BYTE 7
*/
#define REGMODE 0x07
#define FLAGMODENONE B00000000 // none
#define FLAGMODEMAN B00000001  // manual
#define FLAGMODEHOLD B00000010 // Hold
#define FLAGMODEREL B00000100  // relative
#define FLAGMODEAC B00001000   // ac
#define FLAGMODEDC B00010000   // dc
#define FLAGMODEAUTO B00100000 // auto

/*
  6:  MIN MAX
    - dec  0 MIN MAX off
    - dec 16 MIN
    - dec 32 MAX
    BYTE 8
*/
#define REGMINMAX 0x08
#define FLAGMINMAXNONE B00000000
#define FLAGMIN B00010000
#define FLAGMAX B00100000

/*
  7:  Units
    - dec   2   0    % Duty
    - dec   0   1 Fahrenheit
    - dec   0   2 Grad
    - dec   0   4   nF
    - dec   0   8   Hz
    - dec   0  16  hFE
    - dec   0  32  Ohm
    - dec  32  32 kOhm
    - dec  16  32 MOhm
    - dec 128  64   uA
    - dec  64  64   mA
    - dec   0  64    A
    - dec  64 128   mV
    - dec   0 128    V
    BYTES 9 & 10
*/
#define REGSCALE 0x09
#define FLAGSCALEDUTY B00000010
#define FLAGSCALEDIODE B00000100
#define FLAGSCALEBUZZ B00001000
#define FLAGSCALEMEGA B00010000
#define FLAGSCALEKILO B00100000
#define FLAGSCALEMILI B01000000
#define FLAGSCALEMICRO B10000000

#define REGUNIT 0x0a
#define FLAGUNITNONE B00000000
#define FLAGUNITFAHR B00000001
#define FLAGUNITGRAD B00000010
#define FLAGUNITNF B00000100
#define FLAGUNITHZ B00001000
#define FLAGUNITHFE B00010000
#define FLAGUNITOHM B00100000
#define FLAGUNITAMP B01000000
#define FLAGUNITVOLT B10000000

/*
  8:  ???
*/

/*
  9:  CR + LF
*/
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

uint8_t lastValues[replySize - 3];

void initValues(DynamicJsonDocument &json)
{
  lastValues[REGMODE] |= FLAGMODEMAN;
  json["Values"]["Value"] = 0.0;
  // json["Attributes"]["Scale"] = ""; // not used -> sending SI base units
  json["Attributes"]["Unit"] = "V";
  json["Attributes"]["Range"] = "Auto";
  json["Attributes"]["Capture"] = "Normal";
  json["Attributes"]["Coupling"] = "None";
  json["Attributes"]["Hold"] = "Off";
}

void B35TcheckUnit(uint8_t replyBuffer, DynamicJsonDocument &json)
{
  if (lastValues[REGUNIT] != replyBuffer)
  {
    switch (replyBuffer)
    {
    case FLAGUNITFAHR:
      json["Attributes"]["Unit"] = "*F";
      json["Attributes"]["Coupling"] = "None";
      break;
    case FLAGUNITGRAD:
      json["Attributes"]["Unit"] = "*C";
      json["Attributes"]["Coupling"] = "None";
      break;
    case FLAGUNITNF:
      json["Attributes"]["Unit"] = "nF";
      json["Attributes"]["Coupling"] = "None";
      break;
    case FLAGUNITHZ:
      json["Attributes"]["Unit"] = "Hz";
      json["Attributes"]["Coupling"] = "None";
      break;
    case FLAGUNITHFE:
      json["Attributes"]["Unit"] = "hFE";
      json["Attributes"]["Coupling"] = "None";
      break;
    case FLAGUNITOHM:
      json["Attributes"]["Unit"] = "Ohm";
      json["Attributes"]["Coupling"] = "None";
      break;
    case FLAGUNITAMP:
      json["Attributes"]["Unit"] = "A";
      break;
    case FLAGUNITVOLT:
      json["Attributes"]["Unit"] = "V";
      break;
    }
    lastValues[REGUNIT] = replyBuffer;
    DEBUG_MSG_BLE_DEVICES("Unit changed to: %S \n", (const char *)json["Attributes"]["Unit"]);
  }
  return;
}

void B35TcheckScale(uint8_t replyBuffer, DynamicJsonDocument &json)
{
  if (lastValues[REGSCALE] != replyBuffer)
  {
    switch (replyBuffer)
    {
    case FLAGSCALEDUTY:
      // json["Attributes"]["Scale"]  = "%";
      json["Attributes"]["Unit"] = "%";
      break;
    case FLAGSCALEDIODE:
      break;
    case FLAGSCALEBUZZ:
      break;
    case FLAGSCALEMEGA:
      // json["Attributes"]["Scale"]  = "M";
      break;
    case FLAGSCALEKILO:
      // json["Attributes"]["Scale"]  = "k";
      break;
    case FLAGSCALEMILI:
      // json["Attributes"]["Scale"]  = "m";
      break;
    case FLAGSCALEMICRO:
      // json["Attributes"]["Scale"]  = "u";
      break;
    }
    lastValues[REGSCALE] = replyBuffer;
  }
  return;
}

void B35TcheckMeasMode(uint8_t replyBuffer, DynamicJsonDocument &json)
{
  DEBUG_MSG_BLE_DEVICES("replyBuffer: " BYTE_TO_BINARY_PATTERN "\n", BYTE_TO_BINARY(replyBuffer));
  DEBUG_MSG_BLE_DEVICES("lastValues: " BYTE_TO_BINARY_PATTERN "\n", BYTE_TO_BINARY(lastValues[REGMODE]));
  DEBUG_MSG_BLE_DEVICES("New Mode: %s\n", (lastValues[REGMODE] != replyBuffer) ? "true" : "false");

  if (lastValues[REGMODE] != replyBuffer)
  {
    if ((lastValues[REGMODE] & FLAGMODEAUTO) != (replyBuffer & FLAGMODEAUTO))
    { // auto
      if ((replyBuffer & FLAGMODEAUTO) == FLAGMODEAUTO)
      {
        lastValues[REGMODE] |= FLAGMODEAUTO;
        json["Attributes"]["Range"] = "Auto";
      }
      else
      {
        lastValues[REGMODE] &= ~(FLAGMODEAUTO);
        json["Attributes"]["Range"] = "Manual";
      }
      DEBUG_MSG_BLE_DEVICES("Auto Mode: %s\n", (lastValues[REGMODE] & FLAGMODEAUTO) ? "true" : "false");
    }
    if ((lastValues[REGMODE] & FLAGMODEHOLD) != (replyBuffer & FLAGMODEHOLD))
    { // Hold
      if ((replyBuffer & FLAGMODEHOLD) == FLAGMODEHOLD)
      {
        lastValues[REGMODE] |= FLAGMODEHOLD;
        json["Attributes"]["Capture"] = "Hold";
      }
      else
      {
        json["Attributes"]["Capture"] = "Normal";
        lastValues[REGMODE] &= ~(FLAGMODEHOLD);
      }
      DEBUG_MSG_BLE_DEVICES("Hold: %s\n", (lastValues[REGMODE] & FLAGMODEHOLD) ? "true" : "false");
    }
    if ((lastValues[REGMODE] & FLAGMODEREL) != (replyBuffer & FLAGMODEREL))
    { // relative
      if ((replyBuffer & FLAGMODEREL) == FLAGMODEREL)
      {
        lastValues[REGMODE] |= FLAGMODEREL;
        lastValues[REGMODE] &= ~(FLAGMODEMAN);
        json["Attributes"]["Capture"] = "Relative";
      }
      else
      {
        json["Attributes"]["Capture"] = "Normal";
        lastValues[REGMODE] &= ~(FLAGMODEREL);
        lastValues[REGMODE] |= FLAGMODEMAN;
      }
      DEBUG_MSG_BLE_DEVICES("Relative: %s\n", (lastValues[REGMODE] & FLAGMODEREL) ? "true" : "false");
    }
    if ((lastValues[REGMODE] & FLAGMODEDC) != (replyBuffer & FLAGMODEDC))
    { // dc
      if ((replyBuffer & FLAGMODEDC) == FLAGMODEDC)
      {
        lastValues[REGMODE] |= FLAGMODEDC;
        json["Attributes"]["Coupling"] = "DC";
      }
      else
      {
        lastValues[REGMODE] &= ~(FLAGMODEDC);
      }
      DEBUG_MSG_BLE_DEVICES("DC: %s\n", (lastValues[REGMODE] & FLAGMODEDC) ? "true" : "false");
    }
    if ((lastValues[REGMODE] & FLAGMODEAC) != (replyBuffer & FLAGMODEAC))
    { // ac
      if ((replyBuffer & FLAGMODEAC) == FLAGMODEAC)
      {
        lastValues[REGMODE] |= FLAGMODEAC;
        json["Attributes"]["Coupling"] = "AC";
      }
      else
      {
        lastValues[REGMODE] &= ~(FLAGMODEAC);
      }
      DEBUG_MSG_BLE_DEVICES("AC: %s\n", (lastValues[REGMODE] & FLAGMODEAC) ? "true" : "false");
    }

    DEBUG_MSG_BLE_DEVICES("Mode changed to: %S %S \n", (const char *)json["Attributes"]["Capture"], (const char *)json["Attributes"]["Coupling"]);
    return;
  }
}

void B35TcheckMinMaxHold(uint8_t replyBuffer, DynamicJsonDocument &json)
{
  if (lastValues[REGMINMAX] != replyBuffer)
  {
    json["Attributes"]["Hold"] = "Off";
    if ((replyBuffer & FLAGMAX) == FLAGMAX)
    {
      lastValues[REGMINMAX] |= FLAGMAX;
      lastValues[REGMODE] &= ~(FLAGMODEMAN);
      DEBUG_MSG_BLE_DEVICES("Max Hold: %s\n", (lastValues[REGMINMAX] & FLAGMAX) ? "true" : "false");
      json["Attributes"]["Hold"] = "Max";
    }
    else
    {
      lastValues[REGMINMAX] &= ~(FLAGMAX);
      lastValues[REGMODE] |= FLAGMODEMAN;
    }
    if ((replyBuffer & FLAGMIN) == FLAGMIN)
    {
      lastValues[REGMINMAX] |= FLAGMIN;
      lastValues[REGMODE] &= ~(FLAGMODEMAN);
      DEBUG_MSG_BLE_DEVICES("Min Hold: %s\n", (lastValues[REGMINMAX] & FLAGMIN) ? "true" : "false");
      json["Attributes"]["Hold"] = "Min";
    }
    else
    {
      lastValues[REGMINMAX] &= ~(FLAGMIN);
      lastValues[REGMODE] |= FLAGMODEMAN;
    }
    DEBUG_MSG_BLE_DEVICES("Hold changed to: %S \n", (const char *)json["Attributes"]["Hold"]);
  }
  return;
}

float B35TparseValue(uint8_t replyBuffer[])
{
  char digits[6] = {0, 0, 0, 0, 0, 0};
  uint8_t digitOffset = 0;

  if (lastValues[REGPLUSMINUS] != replyBuffer[REGPLUSMINUS])
  { // sign "-" : 0
    lastValues[REGPLUSMINUS] = replyBuffer[REGPLUSMINUS];
  }
  if ((lastValues[REGPLUSMINUS] & FLAGMINUS) == FLAGMINUS)
  {
    digits[0] = '-';
    digitOffset++;
  }

  if (lastValues[REGDIG1] != replyBuffer[REGDIG1])
  { // digit 1 : 0/1
    lastValues[REGDIG1] = replyBuffer[REGDIG1];
  }
  if (lastValues[REGDIG1] >= 0x30 && lastValues[REGDIG1] <= 0x39)
  {
    digits[0 + digitOffset] = lastValues[REGDIG1];
  }

  if (lastValues[REGPOINT] != replyBuffer[REGPOINT])
  { // decimal point
    lastValues[REGPOINT] = replyBuffer[REGPOINT];
  }

  if ((lastValues[REGPOINT] & FLAGPOINT1) == FLAGPOINT1)
  { // decimal point 1 : 1/2
    digits[1 + digitOffset] = '.';
    digitOffset++;
  }

  if (lastValues[REGDIG2] != replyBuffer[REGDIG2])
  {
    lastValues[REGDIG2] = replyBuffer[REGDIG2];
  }
  if (lastValues[REGDIG2] >= 0x30 && lastValues[REGDIG2] <= 0x39)
  {
    digits[1 + digitOffset] = lastValues[REGDIG2]; // digit 2 : 1/2/3
  }

  if (((lastValues[REGPOINT] & FLAGPOINT2) == FLAGPOINT2) && (digitOffset <= 1))
  { // decimal point 2 : 2/3
    digits[2 + digitOffset] = '.';
    digitOffset++;
  }

  if (lastValues[REGDIG3] != replyBuffer[REGDIG3])
  {
    lastValues[REGDIG3] = replyBuffer[REGDIG3];
  }
  if (lastValues[REGDIG3] >= 0x30 && lastValues[REGDIG3] <= 0x39)
  {
    digits[2 + digitOffset] = lastValues[REGDIG3]; // digit 3 : 2/3/4
  }

  if (((lastValues[REGPOINT] & FLAGPOINT3) == FLAGPOINT3) && (digitOffset <= 1))
  { // decimal point 3/4
    digits[3 + digitOffset] = '.';
    digitOffset++;
  }

  if (lastValues[REGDIG4] != replyBuffer[REGDIG4])
  {
    lastValues[REGDIG4] = replyBuffer[REGDIG4];
  }

  if (lastValues[REGDIG4] >= 0x30 && lastValues[REGDIG4] <= 0x39)
  {
    digits[3 + digitOffset] = lastValues[REGDIG4]; // digit 3/4/5
  }

  float numValue = atof(digits);
  return numValue;
}

float B35TconvertValue(float measurement, uint8_t scale, bool backward = false)
{
  if (backward == true)
  {
    switch (scale)
    {
    case FLAGSCALEMEGA:
      return measurement / 1e6;
      break;
    case FLAGSCALEKILO:
      return measurement / 1e3;
      break;
    case FLAGSCALEMILI:
      return measurement * 1e3;
      break;
    case FLAGSCALEMICRO:
      return measurement * 1e6;
      break;
    default:
      return measurement;
      break;
    }
  }
  else
  {
    switch (scale)
    {
    case FLAGSCALEMEGA:
      return measurement * 1e6;
      break;
    case FLAGSCALEKILO:
      return measurement * 1e3;
      break;
    case FLAGSCALEMILI:
      return measurement / 1e3;
      break;
    case FLAGSCALEMICRO:
      return measurement / 1e6;
      break;
    default:
      return measurement;
      break;
    }
  }
}

bool parseValues(uint8_t replyBuffer[], DynamicJsonDocument &json)
{
  B35TcheckScale(replyBuffer[REGSCALE], json);
  B35TcheckUnit(replyBuffer[REGUNIT], json);
  B35TcheckMeasMode(replyBuffer[REGMODE], json);
  B35TcheckMinMaxHold(replyBuffer[REGMINMAX], json);
  float parsedValue = B35TparseValue(replyBuffer);
  json["Values"]["Value"] = B35TconvertValue(parsedValue, replyBuffer[REGSCALE]);
  if((const char *)json["Attributes"]["Unit"] == "Ohm" && (float)json["Values"]["Value"] == 0.0){
    return false;
  }
  // DEBUG_MSG_BLE_DEVICES("normalized Value: %4.3f \n", normalizedValue);
  // json["Values"]["Value"] = convertedValue;
  // DEBUG_MSG_BLE_DEVICES("json[Values][value]: %4.4f \n", json["Values"]["value"].as<float>());
  Serial.printf("%.4f %s | Coupling: %s | Mode: %s | Holding: %s | Range: %s \n", (float)json["Values"]["Value"], (const char *)json["Attributes"]["Unit"], (const char *)json["Attributes"]["Coupling"], (const char *)json["Attributes"]["Capture"], (const char *)json["Attributes"]["Hold"], (const char *)json["Attributes"]["Range"]);
  // DEBUG_MSG_BLE_DEVICES("%4.3f %S \n", json["value"], json["Unit"]);
  return true;
}

#elif defined(BLE_DEVICE_ALLPOWERS)

void initValues(DynamicJsonDocument &json)
{
  DEBUG_MSG_BLE_DEVICES("init Values..");
  json["Values"]["DC Output"] = 0;
  json["Values"]["AC Output"] = 0;
  json["Values"]["LED"] = 0;
  json["Values"]["Battery Charge"] = 0;
  json["Values"]["Power Input"] = 0;
  json["Values"]["Power Output"] = 0;
  json["Values"]["Remaining Hours"] = 0;
  json["Values"]["Remaining Minutes"] = 0;
  json["Values"]["Remaining Minutes complete"] = 0;
}

void calcOutputStatus(uint8_t dataByte, DynamicJsonDocument &json)
{
  json["Values"]["DC Output"] = bitRead(dataByte, 0);
  json["Values"]["AC Output"] = bitRead(dataByte, 1);
  json["Values"]["LED"] = bitRead(dataByte, 4);
  DEBUG_MSG_BLE_DEVICES("Output: DC:%d AC:%d LED:%d \n", (int)json["Values"]["DC Output"], (int)json["Values"]["AC Output"], (int)json["Values"]["LED"]);
}

void calcBatteryCharge(uint8_t dataByte, DynamicJsonDocument &json)
{
  json["Values"]["Battery Charge"] = dataByte;
  DEBUG_MSG_BLE_DEVICES("Battery Charge: %d \n", (int)json["Values"]["Battery Charge"]);
}

void calcInputPower(uint8_t lowByte, uint8_t highByte, DynamicJsonDocument &json)
{
  json["Values"]["Power Input"] = (lowByte & 0xFF) + (highByte << 8);
  DEBUG_MSG_BLE_DEVICES("Power Input: %d \n", (int)json["Values"]["Power Input"]);
}

void calcOutputPower(uint8_t lowByte, uint8_t highByte, DynamicJsonDocument &json)
{
  json["Values"]["Power Output"] = (lowByte & 0xFF) + (highByte << 8);
  DEBUG_MSG_BLE_DEVICES("Power Output: %d \n", (int)json["Values"]["Power Output"]);
}

void calcRemainingRuntime(uint8_t lowByte, uint8_t highByte, DynamicJsonDocument &json)
{
  uint16_t minutes = (lowByte & 0xFF) + (highByte << 8);
  json["Values"]["Remaining Minutes complete"] = minutes;
  json["Values"]["Remaining Hours"] = floor(minutes / 60);
  json["Values"]["Remaining Minutes"] = minutes % 60;
  DEBUG_MSG_BLE_DEVICES("Remaining Runtime: %d:%dh \n", (int)json["Values"]["Remaining Hours"], (int)json["Values"]["Remaining Minutes"]);
}

bool parseValues(uint8_t replyBuffer[], DynamicJsonDocument &json)
{
  DEBUG_MSG_BLE_DEVICES("parsing Values.. \n");
  calcOutputStatus(replyBuffer[7], json);
  calcBatteryCharge(replyBuffer[8], json);
  calcInputPower(replyBuffer[10], replyBuffer[9], json);
  calcOutputPower(replyBuffer[12], replyBuffer[11], json);
  calcRemainingRuntime(replyBuffer[14], replyBuffer[13], json);

  // printBuffer(replyBuffer);
  return true;
}
#endif