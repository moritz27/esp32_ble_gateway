#include <Arduino.h>
#include <ArduinoJson.h>
#include "debug_interface.h"
#include "gateway_config.h"
#include "wifi_connection.h"
#include "ble_interface.h"
#include "ble_devices.h"
#include "led_interface.h"
#if defined(USE_WIFI_MULTI)
WiFiMulti wifiMulti;
#elif defined(USE_WIFI)
WiFiClient wifiClient;
#endif
#if defined(OTA)
#include "ota_setup.h"
#endif
#if defined(MQTT)
#include "mqtt_interface.h"
PubSubClient mqttClient(MQTT_SERVER, 1883, wifiClient); // 1883 is the listener port for the Broker
#endif
#if defined(INFLUXDB)
#include "influx_interface.h"
#endif
#if defined(LCD_DISPLAY)
#include "lcd_display_interface.h"
#endif
#if defined(SD_CARD)
#include "sd_card_interface.h"
#include "time_interface.h"
#endif

DynamicJsonDocument json(1024);
static boolean firstNotify = true; // flag first notify after start or reconnect

void setup()
{
  setupLed();
#if defined(LED_DATA_PIN)
  setLedColor(CRGB::Red);
#endif
  // startLedTimer(500);
  Serial.begin(115200);
#if defined(LCD_DISPLAY)
  setupDisplay("ESP32 BLE Gateway");
#endif
#if defined(esp32_c3_pico) || defined(esp32_c3_oled)
#if (WAIT_FOR_SERIAL_IN == 1)
  while (Serial.available() == 0)
  {
    // checkLedstate();
  } // wait for user input to proceed
#endif
#endif
  delay(10);
#if defined(LED_DATA_PIN)
  setLedColor(CRGB::Yellow);
#endif
  DEBUG_MSG("Start BLE Gateway for %s \n", MEASUREMENT_NAME);
#if defined(LCD_DISPLAY)
  reprintText("Begin");
#endif
#if defined(LCD_DISPLAY)
  reprintText("Setup WIFI");
#endif
#if defined(USE_WIFI_MULTI)
  setup_wifi_multi(wifiMulti);
#elif defined(USE_WIFI)
  setup_wifi(wifiClient, WIFI_SSID_DEFAULT, WIFI_PASSWORD_DEFAULT);
#endif

#if defined(MQTT)
#if defined(LCD_DISPLAY)
  reprintText("Setup MQTT");
#endif
  setup_mqtt(mqttClient, MY_HOSTNAME);
#endif
#if defined(OTA)
#if defined(LCD_DISPLAY)
  reprintText("Setup OTA");
#endif
  setup_OTA(MY_HOSTNAME);
#endif
  BLEDevice::init("Gateway");
  initValues(json);
#if defined(SD_CARD)
  setupSdCard();
  writeCsvHeaderFromJsonToSd("/data.csv", json);
  initTime();
#endif
}

void loop()
{
#if defined(OTA)
  ArduinoOTA.handle();
#endif
  if (getConnectionStatus() == false)
  {
#if defined(LCD_DISPLAY)
    reprintText("Scan for BLE Device");
#endif
    delay(250);
    if (getScanStartTime() != 0 && millis() > (getScanStartTime() + (scanTime * 1000)))
    {
      setScan(0);
      return;
    }

    if (getScanStartTime() == 0)
    {
      if (firstNotify == false)
      {
        Serial.println("First Notify");
      }
      else
        doScan();
      return;
    }

    setScan(0);
    if (connectToServer())
    {
      setLastBleNotifyTime(millis());
    }
  }
  else
  {

    if (millis() > (getLastBleNotifyTime() + maxWaitForNotify) && firstNotify == false)
    {
      DEBUG_MSG("No notify from %s (>%d)\n", DEVICENAME, maxWaitForNotify);
      return;
    }

    if (newBleDataAvailable() == true)
    {
#if defined(LED_PIN)
      digitalWrite(LED_PIN, HIGH);
#elif LED_DATA_PIN
      setLedColor(CRGB::Blue);
#endif
      if (parseValues(getReplyBuffer(), json))
      {
#if defined(SD_CARD)
        writeCsvLineFromJsonToSd("/data.csv", getTime(), json);
#endif

#if defined(MQTT)
        send_mqtt_json(mqttClient, MY_HOSTNAME, json, TOPIC);
#endif
#if defined(INFLUXDB)
#if defined(LCD_DISPLAY)
        reprintText("Writing Points to InfluxDB");
#endif
        writePointsToInflux(json);
#endif
      }
#if defined(LED_PIN)
      digitalWrite(LED_PIN, LOW);
#elif LED_DATA_PIN
      setLedColor(CRGB::Green);
#endif
      resetBleDataAvailable();
    }
  }
}
