#include "ota_setup.h"

void setup_OTA(const char* hostname)
{
  DEBUG_MSG_OTA("Setup OTA: ");
  ArduinoOTA.onStart([]()
                     {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_FS
      type = "filesystem";
    }
    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    DEBUG_MSG_OTA("Start updating "); });
  ArduinoOTA.onEnd([]()
                   { DEBUG_MSG_OTA("\nEnd \n"); });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total)
                        { DEBUG_MSG_OTA("Progress: %u%%\r", (progress / (total / 100))); });
  ArduinoOTA.onError([](ota_error_t error)
                     {
    DEBUG_MSG_OTA("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      DEBUG_MSG_OTA("Auth Failed \n");
    } else if (error == OTA_BEGIN_ERROR) {
      DEBUG_MSG_OTA("Begin Failed \n");
    } else if (error == OTA_CONNECT_ERROR) {
      DEBUG_MSG_OTA("Connect Failed \n");
    } else if (error == OTA_RECEIVE_ERROR) {
      DEBUG_MSG_OTA("Receive Failed \n");
    } else if (error == OTA_END_ERROR) {
      DEBUG_MSG_OTA("End Failed \n");
    } });

  ArduinoOTA.setHostname(hostname);
  ArduinoOTA.onError([](ota_error_t error)
                     {
    (void)error;
    ESP.restart(); });
  ArduinoOTA.begin();
  DEBUG_MSG_OTA("..done \n");
}
