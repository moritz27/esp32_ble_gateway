#include "time_interface.h"

void initTime(){
    configTime(0, 0 , NTP_SERVER);
}

unsigned long getTime() {
  time_t now;
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo, 1000)) {
    //Serial.println("Failed to obtain time");
    return(0);
  }
  time(&now);
  return now;
}